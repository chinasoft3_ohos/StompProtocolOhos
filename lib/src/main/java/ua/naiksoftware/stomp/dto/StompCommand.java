package ua.naiksoftware.stomp.dto;

/**
 * Created by naik on 05.05.16.
 *
 * @since 2021-04-27
 */
public final class StompCommand {

    /**
     * CONNECT
     */
    public static final String CONNECT = "CONNECT";

    /**
     * CONNECTED
     */
    public static final String CONNECTED = "CONNECTED";

    /**
     * SEND
     */
    public static final String SEND = "SEND";

    /**
     * MESSAGE
     */
    public static final String MESSAGE = "MESSAGE";

    /**
     * SUBSCRIBE
     */
    public static final String SUBSCRIBE = "SUBSCRIBE";

    /**
     * UNSUBSCRIBE
     */
    public static final String UNSUBSCRIBE = "UNSUBSCRIBE";

    /**
     * UNKNOWN
     */
    public static final String UNKNOWN = "UNKNOWN";

    /**
     * 构造
     */
    private StompCommand() {
    }
}
