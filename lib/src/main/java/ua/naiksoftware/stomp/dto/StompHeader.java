package ua.naiksoftware.stomp.dto;

/**
 * Created by naik on 05.05.16.
 *
 * @since 2021-04-21
 */
public class StompHeader {
    /**
     * VERSION
     */
    public static final String VERSION = "accept-version";
    /**
     * HEART_BEAT
     */
    public static final String HEART_BEAT = "heart-beat";
    /**
     * DESTINATION
     */
    public static final String DESTINATION = "destination";
    /**
     * SUBSCRIPTION
     */
    public static final String SUBSCRIPTION = "subscription";
    /**
     * CONTENT_TYPE
     */
    public static final String CONTENT_TYPE = "content-type";
    /**
     * MESSAGE_ID
     */
    public static final String MESSAGE_ID = "message-id";
    /**
     * ID
     */
    public static final String ID = "id";
    /**
     * ACK
     */
    public static final String ACK = "ack";

    private final String mKey;
    private final String mValue;

    /**
     * StompHeader
     *
     * @param key
     * @param value
     */
    public StompHeader(String key, String value) {
        mKey = key;
        mValue = value;
    }

    /**
     * getKey
     *
     * @return String
     */
    public String getKey() {
        return mKey;
    }

    /**
     * getValue
     *
     * @return String
     */
    public String getValue() {
        return mValue;
    }

    /**
     * toString
     *
     * @return toString
     */
    @Override
    public String toString() {
        return "StompHeader{" + mKey + '=' + mValue + '}';
    }
}
