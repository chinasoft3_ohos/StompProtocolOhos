package ua.naiksoftware.stomp.pathmatcher;

import ua.naiksoftware.stomp.dto.StompHeader;
import ua.naiksoftware.stomp.dto.StompMessage;

/**
 * SimplePathMatcher
 *
 * @since 2021-04-27
 */
public class SimplePathMatcher implements PathMatcher {
    /**
     * matches
     * @param path path
     * @param msg msg
     * @return boolean
     */
    @Override
    public boolean matches(String path, StompMessage msg) {
        String dest = msg.findHeader(StompHeader.DESTINATION);
        if (dest == null) {
            return false;
        } else {
            return path.equals(dest);
        }
    }
}
