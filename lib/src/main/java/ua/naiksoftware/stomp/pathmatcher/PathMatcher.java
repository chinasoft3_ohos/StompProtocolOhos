package ua.naiksoftware.stomp.pathmatcher;

import ua.naiksoftware.stomp.dto.StompMessage;

/**
 * PathMatcher
 *
 * @since 2021-04-21
 */
public interface PathMatcher {
    /**
     * matches
     *
     * @param path
     * @param msg
     * @return boolean
     */
    boolean matches(String path, StompMessage msg);
}
