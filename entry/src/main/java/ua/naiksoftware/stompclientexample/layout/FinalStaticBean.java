
/*
 * Copyright (C) 2021 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package ua.naiksoftware.stompclientexample.layout;

import ohos.hiviewdfx.HiLog;
import ohos.hiviewdfx.HiLogLabel;

/**
 * FinalStaticBean 常数标志类
 *
 * @since 2021-03-27
 */
public abstract class FinalStaticBean {
    /**
     * 表示横向网格布局
     */
    public static final int GRIDLAYOUTHORIZONTALTYPE = 1;
    /**
     * 表示纵向网格布局
     */
    public static final int GRIDLAYOUTVERTICALTYPE = 2;
    /**
     * 表示列表布局
     */
    public static final int LINEARLAYOUTTYPE = 3;
    /**
     * 统一HiLogLabel
     */
    public static final HiLogLabel DRAGLISTVIEW = new HiLogLabel(HiLog.LOG_APP, 0x00201, "DragListView");
    /**
     * 表示弧度边框
     */
    public static final int RANDIUBORDER = 0;
    /**
     * 无边框
     */
    public static final int NOBORDER = 1;
    /**
     * 添加布局
     */
    public static final int ADDCOLUMN = 200;
    /**
     * 删除布局
     */
    public static final int REMOVECOLUMN = 410;
    /**
     * 横向列表布局
     */
    public static final int HORIZONTALLISTLAYOUT = 12;
    /**
     * 横向网格布局
     */
    public static final int HORIZONTALGRIDLAYOUT = 18;
    /**
     * 清除所有item
     */
    public static final int CLEARBOARD = 1019;
    /**
     * 横向滑动布局的宽度为屏幕的0.85
     */
    public static final double DISPLAYWIDTH = 0.85;
    /**
     * 左滑标志
     */
    public static final int LEFTMOVE = 4;
    /**
     * 右滑标志
     */
    public static final int RIGHTMOVE = 5;
    /**
     * 允许滑动标志
     */
    public static final int ENABLEDRAG = 6;
    /**
     * 禁止滑动标志
     */
    public static final int DISABLEDRAG = 7;
    /**
     * 上滑标志
     */
    public static final int UPMOVE = 8;
    /**
     * 下滑标志
     */
    public static final int DOWNMOVE = 8;
    /**
     * 代替常数0
     */
    public static final int NUM_0 = 0;
    /**
     * 代替常数1
     */
    public static final int NUM_1 = 1;
    /**
     * 代替常数2
     */
    public static final int NUM_2 = 2;
    /**
     * 代替常数3
     */
    public static final int NUM_3 = 3;
    /**
     * 代替常数4
     */
    public static final int NUM_4 = 4;
    /**
     * 代替常数5
     */
    public static final int NUM_5 = 5;
    /**
     * 代替常数7
     */
    public static final int NUM_7 = 7;
    /**
     * 代替常数12
     */
    public static final int NUM_12 = 12;
    /**
     * 代替常数15
     */
    public static final int NUM_15 = 15;
    /**
     * 代替常数16
     */
    public static final int NUM_16 = 16;
    /**
     * 代替常数20
     */
    public static final int NUM_20 = 20;
    /**
     * 代替常数40
     */
    public static final int NUM_40 = 40;
    /**
     * 代替常数50
     */
    public static final int NUM_50 = 50;
    /**
     * 代替常数58
     */
    public static final int NUM_58 = 58;
    /**
     * 代替常数100
     */
    public static final int NUM_100 = 100;
    /**
     * 代替常数120
     */
    public static final int NUM_120 = 120;
    /**
     * 代替常数120
     */
    public static final int NUM_126 = 126;
    /**
     * 代替常数120
     */
    public static final int NUM_130 = 130;
    /**
     * 代替常数150
     */
    public static final int NUM_150 = 150;
    /**
     * 代替常数198
     */
    public static final int NUM_198 = 198;
    /**
     * 代替常数200
     */
    public static final int NUM_200 = 200;
    /**
     * 代替常数255
     */
    public static final int NUM_255 = 255;
    /**
     * 代替常数400
     */
    public static final int NUM_400 = 400;
    /**
     * 代替常数500
     */
    public static final int NUM_500 = 500;
    /**
     * 代替常数840
     */
    public static final int NUM_840 = 840;
    /**
     * 代替常数1000
     */
    public static final int NUM_1000 = 1000;
    /**
     * 代替常数1800
     */
    public static final int NUM_1800 = 1800;
    /**
     * 代替常数1400
     */
    public static final int NUM_4000 = 4000;
    /**
     * 代替常数5000
     */
    public static final int NUM_5000 = 5000;
    /**
     * 代替常数0.5f
     */
    public static final float NUM_HALF = 0.5f;
    /**
     * 代替常数0.8f
     */
    public static final float NUM_ZERO_EIGHT = 0.8f;
    /**
     * 代替常数0.9f
     */
    public static final float NUM_ZERO_NINE = 0.9f;
    /**
     * 代替常数1f
     */
    public static final float NUM_ONE_FLOAT = 1f;
    /**
     * 代替字符串0
     */
    public static final String ZERO_STRING = "0";
    /**
     * 代替字符串Item
     */
    public static final String ITEM = "Item ";
    /**
     * 代替字符串Test
     */
    public static final String TEST = "Test ";
    /**
     * 代替字符串Test board
     */
    public static final String TEST_LIST = "Test lists";
    /**
     * 代替字符串Disable drag
     */
    public static final String DISABLEDRAG_STRING = "Disable drag";
    /**
     * 代替字符串Grid layout
     */
    public static final String GRIDLAYOUT = "Grid layout";
    /**
     * 代替字符串Add column
     */
    public static final String ADDCOLUMN_STRING = "Add column";
    /**
     * 代替字符串Remove column
     */
    public static final String REMOVECOLUMN_STRING = "Remove column";
    /**
     * 代替字符串Clear board
     */
    public static final String CLEARBOARD_STRING = "Clear board";
    /**
     * Rgb中灰色值
     */
    public static final int GRAY = 198;
    /**
     * Rgb中白色值
     */
    public static final int WHITE = 255;
}
