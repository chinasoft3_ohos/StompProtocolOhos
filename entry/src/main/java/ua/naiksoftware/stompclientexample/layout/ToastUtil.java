/*
 * Copyright (C) 2021 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package ua.naiksoftware.stompclientexample.layout;

import ohos.agp.components.*;
import ohos.agp.utils.LayoutAlignment;
import ohos.agp.window.dialog.ToastDialog;
import ohos.app.Context;

/**
 * toast工具类，可快速实现toast效果，支持设置圆角弧度，x，y轴偏移和权重方式
 *
 * @since 2021-04-19
 */
public class ToastUtil {
    private static int radius = FinalStaticBean.NUM_58;
    private Context context;

    /**
     * Toast构造器
     *
     * @param cot cot
     */
    public ToastUtil(Context cot) {
        this.context = cot;
    }

    /**
     * 支持静态方法，传入上下文和需要显示的文字
     *
     * @param ctx  ctx
     * @param text text
     */
    public static void toastCtx(Context ctx, String text) {
        new ToastDialog(ctx)
                .setText(text)
                .setSize(ComponentContainer.LayoutConfig.MATCH_PARENT, ComponentContainer.LayoutConfig.MATCH_CONTENT)
                .setAutoClosable(true)
                .setDuration(FinalStaticBean.NUM_5000)
                .setCornerRadius(radius)
                .show();
    }

    /**
     * 支持静态方法，传入上下文和需要布局
     *
     * @param ctx    ctx
     * @param layout layout
     */
    public static void toastCtx(Context ctx, Component layout) {
        Component customToastLayout = (Component) LayoutScatter.getInstance(ctx).parse(layout.getId(), null, false);
        ToastDialog toastDialog = new ToastDialog(ctx);
        toastDialog.setComponent(customToastLayout);
        toastDialog.setCornerRadius(radius);
        toastDialog.setSize(DirectionalLayout.LayoutConfig.MATCH_CONTENT, DirectionalLayout.LayoutConfig.MATCH_CONTENT);
        toastDialog.setAlignment(LayoutAlignment.CENTER);
        toastDialog.show();
    }

    /**
     * 支持静态方法，传入需要显示的文字
     *
     * @param text text
     */
    public void toast(String text) {
        new ToastDialog(context)
                .setText(text)
                .setSize(ComponentContainer.LayoutConfig.MATCH_PARENT, ComponentContainer.LayoutConfig.MATCH_CONTENT)
                .setAutoClosable(true)
                .setDuration(FinalStaticBean.NUM_5000)
                .setCornerRadius(radius)
                .show();
    }

    /**
     * 支持静态方法，传入需要显示的文字和圆角角度
     *
     * @param text     text
     * @param duration duration
     */
    public void toast(String text, int duration) {
        new ToastDialog(context)
                .setText(text)
                .setSize(ComponentContainer.LayoutConfig.MATCH_PARENT, ComponentContainer.LayoutConfig.MATCH_CONTENT)
                .setDuration(duration)
                .setCornerRadius(radius)
                .show();
    }

    /**
     * 支持静态方法，传入需要显示的文字，圆角，X轴偏移，Y轴偏移，权重
     *
     * @param text     text
     * @param duration duration
     * @param offsetX  offsetX
     * @param offsetY  offsetY
     * @param gravity  gravity
     */
    public void toast(String text, int duration, int offsetX, int offsetY, int gravity) {
        new ToastDialog(context)
                .setText(text)
                .setSize(ComponentContainer.LayoutConfig.MATCH_PARENT, ComponentContainer.LayoutConfig.MATCH_CONTENT)
                .setAlignment(gravity)
                .setDuration(duration)
                .setCornerRadius(radius)
                .setOffset(offsetX, offsetY)
                .show();
    }
}
