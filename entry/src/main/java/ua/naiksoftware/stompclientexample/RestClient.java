package ua.naiksoftware.stompclientexample;

import retrofit2.Retrofit;
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * Created by Naik on 24.02.17.
 *
 * @since 2021-04-27
 */
public final class RestClient {

    /**
     * 本机IP地址  测试用
     */
    public static final String OHOS_EMULATOR_LOCALHOST = getIp();
    /**
     * 链接端口号
     */
    public static final String SERVER_PORT = "8080";

    private static RestClient instance;
    private static final Object OBJECT_LOCK = new Object();
    private final ExampleRepository mExampleRepository;

    private RestClient() {
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl("http://" + OHOS_EMULATOR_LOCALHOST + ":" + SERVER_PORT + "/")
                .addConverterFactory(GsonConverterFactory.create())
                .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                .build();
        mExampleRepository = retrofit.create(ExampleRepository.class);
    }

    /**
     * getInstance
     *
     * @return RestClient
     */
    public static RestClient getInstance() {
        RestClient restClient = RestClient.instance;
        if (restClient == null) {
            synchronized (OBJECT_LOCK) {
                restClient = RestClient.instance;
                if (restClient == null) {
                    restClient = new RestClient();
                    RestClient.instance = restClient;
                }
            }
        }
        return instance;
    }

    /**
     * getIp
     *
     * @return String
     */
    public static String getIp() {
        String ip = "192.";
        String ip2 = "168.";
        String ip3 = "3.";
        String ip4 = "42";
        return ip + ip2 + ip3 + ip4;
    }

    public ExampleRepository getExampleRepository() {
        return mExampleRepository;
    }
}
