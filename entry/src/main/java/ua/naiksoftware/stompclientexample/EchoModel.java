package ua.naiksoftware.stompclientexample;

/**
 * Created by Naik on 24.02.17.
 *
 * @since 2021-04-27
 */
public class EchoModel {

    private String echo;

    /**
     * 构造函数
     */
    public EchoModel() {
    }

    /**
     * getEcho
     *
     * @return String
     */
    public String getEcho() {
        return echo;
    }

    /**
     * setEcho
     *
     * @param str echo
     */
    public void setEcho(String str) {
        this.echo = str;
    }
}
