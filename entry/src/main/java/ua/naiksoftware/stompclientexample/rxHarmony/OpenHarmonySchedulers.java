package ua.naiksoftware.stompclientexample.rxHarmony;

import io.reactivex.Scheduler;
import ohos.eventhandler.EventHandler;
import ohos.eventhandler.EventRunner;
import ohos.hiviewdfx.HiLog;
import ohos.hiviewdfx.HiLogLabel;


/**
 * HmOs-specific Schedulers.
 *
 * @since 2021-04-27
 */
public final class OpenHarmonySchedulers {
    private static final HiLogLabel LABEL = new HiLogLabel(0, 0, "HarmonySchedulers");

    private static final Scheduler MAIN_THREAD = RxHmOSPlugins.initMainThreadScheduler(
            () -> MainHolder.DEFAULT);

    /**
     * 构造函数
     */
    private OpenHarmonySchedulers() {
        throw new AssertionError("No instances.");
    }

    /**
     * 主线程
     * *
     *
     * @return Scheduler
     */
    public static Scheduler mainThread() {
        return RxHmOSPlugins.onMainThreadScheduler(MAIN_THREAD);
    }


    /**
     * from 根据 eventRunner 返回 Scheduler
     *
     * @param eventRunner eventRunner
     * @return Scheduler
     */
    public static Scheduler from(EventRunner eventRunner) {
        if (eventRunner == null) {
            HiLog.debug(LABEL, "eventRunner == null");
            return null;
        } else {
            return new OpenHandlerScheduler(new EventHandler(eventRunner));
        }
    }

    /**
     * MainHolder
     *
     * @since 2021-03-29
     */
    private static final class MainHolder {
        static final Scheduler DEFAULT = new OpenHandlerScheduler(new EventHandler(EventRunner.getMainEventRunner()));
    }


}

