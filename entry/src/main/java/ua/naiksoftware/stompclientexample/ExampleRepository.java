package ua.naiksoftware.stompclientexample;

import io.reactivex.Completable;
import retrofit2.http.POST;
import retrofit2.http.Query;

/**
 * Created by Naik on 24.02.17.
 *
 * @since 2021-04-21
 */
public interface ExampleRepository {
    /**
     * Completable sendRestEcho
     *
     * @param message
     * @return Completable
     *
     */
    @POST("hello-convert-and-send")
    Completable sendRestEcho(@Query("msg") String message);
}
